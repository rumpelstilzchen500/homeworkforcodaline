package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {

        System.out.print("Введите код товара от 1 до 7: ");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String str = br.readLine().trim();
        int codeOfProductParseInt = Integer.parseInt(str);

        switch (codeOfProductParseInt){
            case 1:
                System.out.println("Мороженко");
                break;
            case 2:
                System.out.println("Мяско");
                break;
            case 3:
                System.out.println("Рыбка");
                break;
            case 4:
                System.out.println("Молочко");
                break;
            case 5:
                System.out.println("Чебурек");
                break;
            case 6:
                System.out.println("Хлебушек");
                break;
            case 7:
                System.out.println("Мороженко");
                break;
            default:
                System.out.println("Нету такого продукта");
                break;
        }

    }
}
